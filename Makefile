default: build

build: clean install buildjs buildcss

install:
	yarn

buildjs:
	lein build

buildcss:
	yarn build

clean:
	rm -rf resources/public/css/*
	rm -rf resources/public/components
	rm -rf resources/public/js/*
	rm -rf node_modules
	rm -rf target
	rm -rf figwheel_server.log

deploy:
	aws s3 sync resources/public/ s3://lursa.imposter.cz --delete
	aws cloudfront create-invalidation --distribution-id E1X59ZJEY2TTBZ --paths "/*"
