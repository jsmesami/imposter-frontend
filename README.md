# Imposter Frontend

The frontend part of automated poster generation system for 
Municipal Library of Prague.

## Overview

A standalone repository for single-page JS application consuming 
[backend](https://gitlab.com/jsmesami/imposter-backend) API. The app is written in 
[ClojureScript](https://clojurescript.org/) for better maintainability.

![Screenshot](/docs/screenshot.png "Running application")

## Development

### Prerequisites

* Node 8+, Yarn
* Java 8+, [Leiningen](https://leiningen.org/) 2.9+

### Environment

To get an interactive development environment for ClojureScript run:

    lein dev

and open your browser at [0.0.0.0:3449](http://0.0.0.0:3449/).
This will auto compile and send all changes to the browser without the
need to reload. 

To start working on Sass files run:

    yarn dev

This will watch Sass sources for changes and feed compiled stylesheets, 
including sourcemaps, into the browser.

## Test

### Prerequisites

Firefox (chosen over Chrome because I don't have one)

    lein test

## Build

To create a production build run:

    make

To clean all installed and compiled files:

    make clean

## License

Copyright © 2019 Ondřej Nejedlý

Distributed under the Eclipse Public License either version 1.0 or 
(at your option) any later version.
