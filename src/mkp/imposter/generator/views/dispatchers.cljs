(ns mkp.imposter.generator.views.dispatchers
  (:require
    [re-frame.core :refer [dispatch]]
    [mkp.imposter.settings :refer [max-image-length]]
    [mkp.imposter.utils.geometry :refer [fit-rect-to-container]]
    [mkp.imposter.utils.image :refer [crop resize canvas->data-url file->base64]]
    [mkp.imposter.utils.string :refer [prepos]]))


(defn- update-field-dispatcher
  [field-id kvs]
  (doseq [[key value] kvs]
    (dispatch [:generator/update-form-field field-id key value])))


(defn- constrain-image-size
  [img]
  (let [img-w (.-width img)
        img-h (.-height img)
        [new-w new-h] (fit-rect-to-container img-w img-h max-image-length max-image-length)]
    (if (> (max img-w img-h) max-image-length)
      (canvas->data-url
        (resize img new-w new-h))
      (.-src img))))


(defn- crop-main-image
  [field-id [target-w target-h] crop-y-pct img]
  (let [img-w (.-width img)
        img-h (.-height img)
        img-ratio (/ img-h img-w)
        crop-ratio (/ target-h target-w)
        default-cyp (fn [i-h c-h] (- 50 (* 50 (/ c-h i-h))))]
    (canvas->data-url
      (if (> img-w max-image-length)
        ;; Scale down and crop scaled
        (let [new-w max-image-length
              new-h (* max-image-length img-ratio)
              [crop-w crop-h] (fit-rect-to-container new-w (* new-w crop-ratio) new-w new-h)
              cyp (or crop-y-pct (default-cyp new-h crop-h))
              _ (dispatch [:generator/update-original-image field-id {:crop-y-pct cyp}])
              crop-x (/ (- new-w crop-w) 2)
              crop-y (* cyp (/ new-h 100))]
          (-> img
              (resize new-w new-h)
              (crop crop-w crop-h crop-x crop-y)))
        ;; Just crop
        (let [[crop-w crop-h] (fit-rect-to-container img-w (* img-w crop-ratio) img-w img-h)
              cyp (or crop-y-pct (default-cyp img-h crop-h))
              _ (dispatch [:generator/update-original-image field-id {:crop-y-pct cyp}])
              crop-x (/ (- img-w crop-w) 2)
              crop-y (* cyp (/ img-h 100))]
          (-> img
              (crop crop-w crop-h crop-x crop-y)))))))


(defn- store-image
  [field-id filename img]
  (update-field-dispatcher field-id
    {:data img
     :filename filename
     :error nil
     :changed true}))


(defn- store-original-image
  [field-id filename target-size img]
  (dispatch [:generator/update-original-image field-id {:target-size target-size
                                                        :crop-y-pct nil
                                                        :filename filename
                                                        :img img}])
  img)


(defn- prepare-image
  [field-id filename base64-data]
  (doto (js/Image.)
    (-> .-src (set! base64-data))
    (-> .-onload (set! #(->> (.-target %)
                             constrain-image-size
                             (store-image field-id filename))))))


(defn- prepare-main-image
  [field-id filename target-size base64-data]
  (doto (js/Image.)
    (-> .-src (set! base64-data))
    (-> .-onload (set! #(->> (.-target %)
                             (store-original-image field-id filename target-size)
                             (crop-main-image field-id target-size nil)
                             (store-image field-id filename))))))


(defn on-change-crop-dispatcher
  [field-id filename target-size crop-y-pct img]
  (->> img
       (crop-main-image field-id target-size crop-y-pct)
       (store-image field-id filename)))


(defn on-change-image-dispatcher
  [field-id img-file target-size]
  (case field-id
    :main_image
    (file->base64 img-file (partial prepare-main-image field-id (.-name img-file) target-size))
    ;; else
    (file->base64 img-file (partial prepare-image field-id (.-name img-file)))))


(defn on-change-text-dispatcher
  [field-id text]
  (update-field-dispatcher field-id {:text (prepos text)
                                     :changed true}))
