(ns mkp.imposter.generator.subs
  (:require
    [re-frame.core :refer [reg-sub]]))


(reg-sub
  :generator/form
  (fn [db _]
    (get-in db [:generator :form])))


(reg-sub
  :generator/orig-image
  (fn [db [_ field-id]]
    (get-in db [:generator :orig-image field-id])))
