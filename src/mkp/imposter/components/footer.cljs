(ns mkp.imposter.components.footer)


(defn footer
  []
  [:div.footer
   "V případě technických potíží se obraťte na "
   [:a {:href "mailto:simona.douskova@mlp.cz"}
    "simona.douskova@mlp.cz"]])
