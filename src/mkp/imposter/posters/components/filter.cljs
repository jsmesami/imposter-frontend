(ns mkp.imposter.posters.components.filter
  (:require
    [clojure.string :refer [join]]
    [reagent.core :as reagent]
    [re-frame.core :refer [dispatch subscribe]]
    [mkp.imposter.components.basic :refer [button input select]]
    [mkp.imposter.resources.core :refer [resource->options]]
    [mkp.imposter.utils.core :refer [remove-prefix]]))


(defn filter-since
  [f* loading?]
  [:div.col-auto
   [:label "Vytvořeno od:"
    [input
     :type "date"
     :value (:since @f*)
     :classes ["form-control"]
     :enabled? (not loading?)
     :on-change #(swap! f* assoc :since %)]]])


(defn filter-until
  [f* loading?]
  [:div.col-auto
   [:label "Vytvořeno do:"
    [input
     :type "date"
     :value (:until @f*)
     :classes ["form-control"]
     :enabled? (not loading?)
     :on-change #(swap! f* assoc :until %)]]])


(defn filter-bureau
  [f* loading?]
  [:div.col-auto
   [:label "Pobočka:"
    [select (resource->options (map remove-prefix @(subscribe [:resources/bureau])))
     :value (:bureau @f*)
     :classes ["form-control"]
     :enabled? (not loading?)
     :on-change #(swap! f* assoc :bureau %)]]])


(defn filter-spec
  [f* loading?]
  [:div.col-auto
   [:label "Šablona:"
    [select (resource->options @(subscribe [:resources/spec]))
     :value (:spec @f*)
     :classes ["form-control"]
     :enabled? (not loading?)
     :on-change #(swap! f* assoc :spec %)]]])


(defn filter-button
  [f* loading?]
  [:div.col-auto
   [button "filtrovat"
    :icon-name "search"
    :classes ["form-control"]
    :enabled? (not (or loading? (empty? @f*)))
    :busy? loading?
    :on-click #(dispatch [:posters/update-filter @f*])]])


(defn clear-button
  [f* loading?]
  (when-not (empty? @f*)
    [:div.col-auto
     [button "zrušit"
      :icon-name "cross"
      :classes ["form-control"]
      :enabled? (not loading?)
      :on-click #(do (reset! f* {})
                     (dispatch [:posters/reset-filter]))]]))


(defn download-button
  [f* loading?]
  (when-not (empty? @f*)
    [:div.col-auto
     [button "stáhnout"
      :icon-name "download-csv"
      :classes ["form-control"]
      :modifiers ["light"]
      :enabled? (not (or loading? (empty? @f*)))
      :busy? loading?
      :on-click #(dispatch [:posters/create-csv @f*])]]))


(defn poster-filter
  []
  (let [f* (reagent/atom {})]
    (fn []
      (let [loading? @(subscribe [:net/loading?])]
        [:<> [:div.form-row.mb-4
               (for [[i fun] (map-indexed vector [filter-since filter-until filter-bureau filter-spec])]
                 ^{:key i}
                 [fun f* loading?])]
             [:div.form-row.mb-4
               (for [[i fun] (map-indexed vector [filter-button clear-button download-button])]
                  ^{:key i}
                  [fun f* loading?])]]))))
