(ns mkp.imposter.posters.components.pagination
  (:require
    [re-frame.core :refer [subscribe dispatch]]
    [mkp.imposter.components.basic :refer [button]]
    [mkp.imposter.posters.db :refer [posters-per-page]]))


(defn pagination
  [posters]
  (let [loading? @(subscribe [:net/loading?])
        offset (get-in posters [:filter :offset])
        paginate #(dispatch [:posters/update-filter {:offset %}])]
    [:div.poster-pagination.row
     [:div.col-12
      [button "novější"
       :icon-name "left"
       :on-click #(paginate (- offset posters-per-page))
       :enabled? (and (not loading?) (:prev? posters))
       :busy? loading?]
      [button "starší"
       :icon-name "right"
       :on-click #(paginate (+ offset posters-per-page))
       :enabled? (and (not loading?) (:next? posters))
       :busy? loading?]]]))
