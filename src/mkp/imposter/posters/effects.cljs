(ns mkp.imposter.posters.effects
  (:require
    [clojure.string :refer [join]]
    [reagent.format :refer [format]]
    [re-frame.core :refer [reg-fx]]
    [mkp.imposter.utils.files :refer [download]]))


(reg-fx
  :posters/download-csv
  (fn [rows]
    (let [data (cons ["Datum" "Titul" "Pobočka"] rows)
          text (join "\n" (map (partial apply format "\"%s\";\"%s\";\"%s\"") data))]
      (download "posters.csv" text))))
