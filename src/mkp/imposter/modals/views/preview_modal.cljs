(ns mkp.imposter.modals.views.preview-modal
  (:require
    [mkp.imposter.modals.views.modal :refer [generic-modal]]
    [mkp.imposter.utils.bem :as bem]))


(def module-name "preview-poster")


(defn preview-poster
  [{thumb :thumb}]
  [generic-modal
   [:div {:class module-name}
    [:img.img-thumbnail.mx-auto
     {:class (bem/be module-name "thumb")
      :src thumb}]]])
