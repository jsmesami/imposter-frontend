(ns mkp.imposter.modals.views.choose-crop-modal
  (:require
    [goog.events :refer [listen unlistenByKey]]
    [goog.events.EventType :refer [MOUSEMOVE MOUSEUP TOUCHMOVE TOUCHEND]]
    [reagent.core :as r]
    [re-frame.core :refer [subscribe]]
    [mkp.imposter.modals.views.modal :refer [dismiss! generic-modal]]
    [mkp.imposter.utils.bem :as bem]
    [mkp.imposter.generator.views.dispatchers :refer [on-change-crop-dispatcher]]
    [mkp.imposter.utils.image :refer [img->base64]]))


(def module-name "choose-crop")


(defn- drag-move
  [img crop-h start-y cyp-atom e]
  (let [img-h (.-height img)
        max-y (- img-h crop-h)
        movement (- start-y (.-clientY e))
        offset (cond
                 (< movement 0) 0
                 (> movement max-y) max-y
                 :else movement)
        crop-y-pct (* (/ offset img-h) 100)]
    (reset! cyp-atom crop-y-pct)))


(defn- drag-start
  [move-ev stop-ev crop-h cyp-atom e]
  (let [img (.-target e)
        start-y (or (.-pageY e)
                    (-> e .-changedTouches (aget 0) .-pageX)
                    0)
        move-listener (atom 0)
        stop-listener (atom 0)
        move (partial drag-move img crop-h start-y cyp-atom)
        stop #(do (unlistenByKey @move-listener)
                  (unlistenByKey @stop-listener))]
    (reset! move-listener (listen js/document move-ev move))
    (reset! stop-listener (listen js/document stop-ev stop))
    (set! (.-ondragstart img) (constantly false))))


(def on-mouse-down (partial drag-start MOUSEMOVE MOUSEUP))
(def on-touch-start (partial drag-start TOUCHMOVE TOUCHEND))


(defn choose-crop
  [field-id]
  (let [orig-image (subscribe [:generator/orig-image field-id])
        cyp-atom (r/atom (:crop-y-pct @orig-image))]
    (fn [_field-id]
      (let [{:keys [target-size filename img]} @orig-image
            [target-w target-h] target-size
            width 300
            img-ratio (/ (.-height img) (.-width img))
            img-h (* width img-ratio)
            crop-ratio (/ target-h target-w)
            crop-h (js/Math.floor (* width crop-ratio))
            crop-y (js/Math.ceil (* @cyp-atom (/ img-h 100)))]
        [generic-modal
         [:div
          {:class module-name}
          [:div
           {:class (bem/be module-name :container)
            :style {:width width
                    :height crop-h}}
           [:img
            {:class (bem/be module-name :image)
             :style {:top (- crop-y)}
             :src (img->base64 img)
             :draggable "false"
             :on-mouse-down (partial on-mouse-down crop-h cyp-atom)
             :on-touch-start (partial on-touch-start crop-h cyp-atom)}]]
          [:button.btn.btn-primary.btn-sm.mt-3
           {:class (bem/be module-name :save)
            :on-click #(do (on-change-crop-dispatcher field-id filename target-size @cyp-atom img)
                           (dismiss! %))}
           "Uložit"]]]))))
