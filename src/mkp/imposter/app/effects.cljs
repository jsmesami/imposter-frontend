(ns mkp.imposter.app.effects
  (:require
    [re-frame.core :refer [dispatch reg-fx]]
    [bidi.bidi :as bidi]
    [pushy.core :as pushy]
    [mkp.imposter.app.db :refer [app-routes]]))


(def severity->log-fn
  {:default js/console.log
   :info    js/console.info
   :warn    js/console.warn
   :error   js/console.error})


(reg-fx
  :app/log
  (fn [[message severity]]
    (severity->log-fn (or severity :default)) message))


(defonce history
  (pushy/pushy #(dispatch [:views/set (:handler %)])
               (partial bidi/match-route app-routes)))


(reg-fx
  :app/init-history
  (fn []
    (pushy/start! history)))


(reg-fx
  :app/navigate
  (fn [handler]
    (pushy/set-token! history (bidi/path-for app-routes handler))))
