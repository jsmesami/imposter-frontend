(ns mkp.imposter.preload
  (:require
    [mkp.imposter.utils.debug :refer [log]]))


(set! (.-log js/window) log)


(enable-console-print!)
