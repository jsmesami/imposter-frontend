(ns mkp.imposter.utils.image)


(defn resize
  [img width height]
  (let [canvas (doto (js/document.createElement "canvas")
                 (-> .-width (set! width))
                 (-> .-height (set! height)))
        context (.getContext canvas "2d")]
    (.drawImage context img 0 0 width height)
    canvas))


(defn crop
  [img w h x y]
  (let [img-w (.-width img)
        img-h (.-height img)
        canvas (doto (js/document.createElement "canvas")
                 (-> .-width (set! w))
                 (-> .-height (set! h)))
        context (.getContext canvas "2d")]
    (.drawImage context img (- x) (- y) img-w img-h)
    canvas))


(defn canvas->data-url
  [canvas]
  (.toDataURL canvas "image/jpeg" 0.65))


(defn file->base64
  [file callback]
  (let [reader (js/FileReader.)]
    (set! (.-onload reader) #(callback (-> % .-target .-result)))
    (.readAsDataURL reader file)))


(defn img->base64
  [img]
  (let [img-w (.-width img)
        img-h (.-height img)
        canvas (doto (js/document.createElement "canvas")
                 (-> .-width (set! img-w))
                 (-> .-height (set! img-h)))
        context (.getContext canvas "2d")]
    (.drawImage context img 0 0)
    (canvas->data-url canvas)))
