(ns mkp.imposter.utils.core
  (:require
    [clojure.string :as s]))


(defn inc-loading-count
  [db]
  (update-in db [:net :loading-count] inc))


(defn dec-loading-count
  [db]
  (update-in db [:net :loading-count] #(if (pos? %) (dec %) 0)))


(defn remove-prefix
  "Removes 'Pobočka' from name in bureau map"
  [bureau]
  (update bureau :name #(s/replace % "Pobočka " "")))
