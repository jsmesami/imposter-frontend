(ns mkp.imposter.utils.geometry)


(defn fit-rect-to-container
  [rect-w rect-h cont-w cont-h]
  (let [cont-ratio (/ cont-w cont-h)
        rect-ratio (/ rect-w rect-h)
        scale-ratio (if (>= rect-ratio cont-ratio)
                      (/ cont-w rect-w)
                      (/ cont-h rect-h))]
    [(* rect-w scale-ratio) (* rect-h scale-ratio)]))
