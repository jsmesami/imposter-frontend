(ns mkp.imposter.utils.files)


(defn download
  [filename text]
  (doto (js/document.createElement "a")
    (.setAttribute "href" (str "data:text/plain;charset=utf-8," (js/encodeURIComponent text)))
    (.setAttribute "download" filename)
    (-> .-style .-display (set! "none"))
    (js/document.body.appendChild)
    (.click)
    (js/document.body.removeChild)))
