(ns mkp.imposter.net.events
  (:require
    [ajax.core :as ajax]
    [clojure.string :as s]
    [day8.re-frame.http-fx]
    [re-frame.core :refer [reg-event-fx trim-v]]
    [mkp.imposter.alert.core :refer [status->kind]]
    [mkp.imposter.utils.core :refer [inc-loading-count dec-loading-count]]
    [mkp.imposter.settings :refer [default-request-timeout api-server]]))


(defn default-success-fx
  [db _]
  {:db db})


(defn failure-message
  "Thanks, DRF!"
  [response]
  (if-not (map? response)
    "Něco se pokazilo."
    (or (:status-text response)
        (:detail response)
        ;; Non-field errors
        (when-let [non-field (:non_field_errors response)]
          (cond->> non-field
            (sequential? non-field) (s/join "\n")
            :default str))
        ;; Field errors
        (->> (map vector
                  (->> response keys (map name))
                  (->> response vals (map #(cond->> % (sequential? %) (s/join ", ") :default str))))
             (map (partial s/join ": "))
             (s/join "\n")))))


(defn default-failure-fx
  [db response]
  {:db db
   :app/log [(str "Request error: " (:debug-message response)) :error]
   :dispatch [:alert/add-message (failure-message response) (-> response :status status->kind)]})


(reg-event-fx
  :net/json-xhr
  [trim-v]
  (fn [{db :db} [method uri & {:keys [data success-fx failure-fx timeout]
                               :or {data {}
                                    success-fx default-success-fx
                                    failure-fx default-failure-fx
                                    timeout default-request-timeout}}]]
    {:db (inc-loading-count db)
     :http-xhrio {:method          method
                  :uri             (str api-server uri)
                  :timeout         timeout
                  :params          data
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:net/success success-fx]
                  :on-failure      [:net/failure failure-fx]}}))


(reg-event-fx
  :net/success
  [trim-v]
  (fn [{:keys [db]} [success-fx response]]
    (-> (dec-loading-count db)
        (success-fx (js->clj response)))))


(reg-event-fx
  :net/failure
  [trim-v]
  (fn [{:keys [db]} [failure-fx response]]
    (-> (dec-loading-count db)
        (failure-fx (js->clj response)))))
