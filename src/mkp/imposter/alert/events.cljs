(ns mkp.imposter.alert.events
  (:require
    [re-frame.core :refer [dispatch reg-event-db reg-event-fx trim-v]]))


(reg-event-fx
  :alert/add-message
  [trim-v]
  (fn [{:keys [db]} [text kind & {:keys [timeout children]}]]
    (let [id (hash text)]
      (when timeout
        (js/setTimeout #(dispatch [:alert/remove-message id]) timeout))
      {:db (assoc-in db [:alert :messages id] {:kind kind, :text text, :children children})})))


(reg-event-db
  :alert/remove-message
  [trim-v]
  (fn [db [id]]
    (update-in db [:alert :messages] dissoc id)))
