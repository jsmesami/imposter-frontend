(ns mkp.imposter.views.events
  (:require
    [re-frame.core :refer [reg-event-fx trim-v]]))


(defn handler->effects
  [db handler]
  (case handler
    :home {:db (assoc db :view :home)
           :dispatch [:posters/reload]}
    :edit {:db (assoc db :view :edit)}
    {:db db}))


(reg-event-fx
  :views/set
  [trim-v]
  (fn [{db :db} [handler]]
    (handler->effects db handler)))
