(ns tests.test-fail-msg
  (:require
    [cljs.test :refer-macros [deftest testing are]]
    [mkp.imposter.net.events :refer [failure-message]]))


(deftest test-failure-message
  (testing ""
    (are [response expected]
      (= expected (failure-message response))
      {:detail "abc"}
      "abc"

      {:non_field_errors ["eenie" "meenie"]}
      "eenie\nmeenie"

      {:non_field_errors "moe"}
      "moe"

      {:non_field_errors 1}
      "1"

      {:name ["is null"]
       :email ["short" "invalid"]
       :passwd 123}
      "name: is null\nemail: short, invalid\npasswd: 123"

      "xyz"
      nil)))
