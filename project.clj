(defproject imposter-frontend "1.1.1"
  :description "Imposter is a poster generation tool for Municipal Library of Prague"

  :url "https://gitlab.com/jsmesami/imposter-frontend"

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.9.1"

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.520"]
                 [org.clojure/core.async  "0.4.500"]
                 [cljs-ajax "0.8.0"]
                 [com.cemerick/url "0.1.1"]
                 [bidi "2.1.6"]
                 [kibu/pushy "0.3.8"]
                 [reagent "0.8.1"]
                 [reagent-utils "0.3.3"]
                 [re-frame "0.10.7"]
                 [day8.re-frame/http-fx "0.1.6"]]

  :plugins [[lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
            [lein-doo "0.1.11"]
            [lein-figwheel "0.5.18"]
            [lein-kibit "0.1.6"]]

  :profiles {:dev {:dependencies [[org.clojure/tools.nrepl "0.2.13"]
                                  [figwheel-sidecar "0.5.18"]
                                  [com.cemerick/piggieback "0.2.2"]
                                  [binaryage/devtools "0.9.10"]
                                  [doo "0.1.11"]
                                  [day8.re-frame/test "0.1.5"]
                                  [day8.re-frame/re-frame-10x "0.4.2"]
                                  ;; Development webserver:
                                  [clj-http "3.10.0"]
                                  [compojure "1.6.0"]
                                  [ring "1.7.1"]
                                  [ring/ring-defaults "0.3.2"]
                                  [tailrecursion/ring-proxy "2.0.0-SNAPSHOT"]]}}

  :source-paths ["src"]

  :clean-targets ^{:protect false} ["resources/public/js" "target"]

  :figwheel {:http-server-root "public"
             :nrepl-port 7002
             :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"]
             :server-port 3449
             :css-dirs ["resources/public/css"] ;; watch and update CSS
             :hawk-options {:watcher :polling} ;; M1 Big Sur workaround
             :ring-handler dev-app/app}

  :doo {:paths {:karma "node_modules/karma/bin/karma"}}

  :cljsbuild {:builds
              [{:id "dev"
                :source-paths ["src"]

                ;; The presence of a :figwheel configuration here will cause figwheel to inject the figwheel client
                ;; into your build
                :figwheel {:on-jsload "mkp.imposter.core/on-js-reload"}
                           ;:open-urls ["http://0.0.0.0:3449"]}

                :compiler {:main mkp.imposter.core
                           :parallel-build true
                           :asset-path "js/out/dev"
                           :output-to "resources/public/js/imposter.js"
                           :output-dir "resources/public/js/out/dev"
                           :source-map-timestamp true
                           :closure-defines {"re_frame.trace.trace_enabled_QMARK_" true}
                           ;; To console.log CLJS data-structures make sure you enable devtools in Chrome
                           ;; https://github.com/binaryage/cljs-devtools
                           :preloads [devtools.preload day8.re-frame-10x.preload mkp.imposter.preload]}}

               {:id "min"
                :source-paths ["src"]
                :compiler {:main mkp.imposter.core
                           :parallel-build true
                           :output-to "resources/public/js/imposter.js"
                           ;:output-dir "resources/public/js/out/min"
                           :optimizations :advanced
                           :closure-defines {mkp.imposter.settings/api-server "https://ovyhn6t1u1.execute-api.eu-central-1.amazonaws.com"
                                             mkp.imposter.settings/api-prefix "/production/"}
                           :pretty-print false}}
               {:id "test"
                :source-paths ["src"]
                :compiler {:output-to "resources/public/js/imposter.js"
                           :output-dir "resources/public/js/out/test"
                           :main tests.core
                           :optimizations :none}}]}

  :aliases {"build" ["do" "cljsbuild" "once" "min"]
            "dev" ["figwheel" "dev"]
            "test" ["doo" "firefox" "test" "once"]})
